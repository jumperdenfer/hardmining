package com.gitlab.jumperdenfer.hardmining.blockChange;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.plugin.Plugin;

/**
 * Change the durability applied on tools after mining an "hard block"
 * @author jumperdenfer
 *
 */
public class BlockHard implements Listener {
	private FileConfiguration config;
	private List<String> hardToBreak;
	private int hardDamage;
	
	public BlockHard(Plugin plugin) {
		this.config = plugin.getConfig();
		this.hardToBreak = this.config.getStringList("hardblock");
		this.hardDamage = this.config.getInt("tooldamage");
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent breakEvent){
		List<Material> HardMaterial = new ArrayList<Material>();
		
		for(String hard : hardToBreak) {
			String[] splittedBlock = hard.split("-");
			HardMaterial.add(Material.getMaterial(splittedBlock[0]));
		}
		if(HardMaterial.contains(breakEvent.getBlock().getType())) {
			this.applyDurability(breakEvent.getPlayer());
		}
	}
	
	/**
	 * Change the durability of the Item in main hand
	 * @param player
	 */
	private void applyDurability(Player player) {
		ItemStack itemToDamage = player.getInventory().getItemInMainHand();
		if((itemToDamage.getType().getMaxDurability() >= 0)) {
			ItemStack damagedItem = this.damageTool(itemToDamage, this.hardDamage);
			player.getInventory().setItemInMainHand(damagedItem);
		}
		
	}
	
	/**
	 * Change the durability of the current Tools;
	 * @param item
	 * @param incommingDamage
	 * @return
	 */
	private ItemStack damageTool(ItemStack item,int incommingDamage) {
		Damageable damageable = (Damageable) item.getItemMeta();
		int currentDmg = damageable.getDamage();
		int newDurability = currentDmg - incommingDamage;
		if(newDurability < 0) {
			return item;
		}
		else {
			damageable.setDamage(incommingDamage);
			item.setItemMeta(damageable);
			return item;
		}
	}
}
