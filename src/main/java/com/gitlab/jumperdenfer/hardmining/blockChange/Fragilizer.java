package com.gitlab.jumperdenfer.hardmining.blockChange;

import java.util.ArrayList;
import java.util.List;


import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.Plugin;

/**
 * Change every surrounding block to a soften version
 * @author jumperdenfer
 *
 */
public class Fragilizer implements Listener {
	private FileConfiguration config;
	private List<Material> fragilizer;
	private List<String> fragilised;
	
	public Fragilizer(Plugin plugin) {
		this.config = plugin.getConfig();
		this.fragilizer = new ArrayList<Material>();
		this.addFragilizer(this.config.getStringList("fragilizer"));
		this.fragilised = this.config.getStringList("fragilised");
		
	}
	
	
	@EventHandler()
	public void onBlockBreak(BlockBreakEvent breakEvent) {
		Block breakblock = breakEvent.getBlock();
		if(this.fragilizer.contains(breakblock.getType())) {
			//Get all block surrounding 
			List<Block> surroundBlock = new ArrayList<Block>();
			for(BlockFace face : BlockFace.values()) {
				surroundBlock.add(breakblock.getRelative(face));
			}
			
			for(Block testblock : surroundBlock ) {
				this.fragiliseBlock(testblock);
			}
			
			
		}
	}
	
	
	private void addFragilizer(List<String> fragilize) {
		for(String fragil : fragilize) {
			Material materialFragilize = Material.getMaterial(fragil);
		    this.fragilizer.add(materialFragilize);
		}
	}
	
	private void fragiliseBlock(Block blockToFragilise) {
		for(String blockFragilise : fragilised) {
			String[] splittedTextFragilse = blockFragilise.split("-");
			Material hard = Material.getMaterial(splittedTextFragilse[0]);
			Material soft = Material.getMaterial(splittedTextFragilse[1]);
			if(blockToFragilise.getType() == hard) {
				blockToFragilise.setType(soft);
			}
		}
	}
}
