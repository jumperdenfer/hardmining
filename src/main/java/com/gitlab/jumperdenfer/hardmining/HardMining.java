package com.gitlab.jumperdenfer.hardmining;

import java.io.File;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.gitlab.jumperdenfer.hardmining.blockChange.BlockHard;
import com.gitlab.jumperdenfer.hardmining.blockChange.Fragilizer;

import net.md_5.bungee.api.ChatColor;

public class HardMining extends JavaPlugin {
	@Override
	public void onEnable() {
		File configFile = new File(getDataFolder(),"config.yml");
		if(!configFile.exists()) {
			this.saveDefaultConfig();
			this.saveResource("config.yml", false);
		}
		PluginManager manager = this.getServer().getPluginManager();
		manager.registerEvents(new BlockHard(this),this);
		manager.registerEvents(new Fragilizer(this),this);
		getLogger().info("onEnable is called!");
	}
	
	@Override
	public void onDisable() {
		getLogger().info("onDisable is called");
	}
	
	public boolean onCommand(CommandSender sender,Command cmd,String label, String[] args) {
		if(label.equalsIgnoreCase("hardmining")){
			if(!sender.hasPermission("hardmining.reload")) {
				sender.sendMessage(ChatColor.RED+ "You cannont run this command");
				return true;
			}
			if(args.length == 0) {
				sender.sendMessage(ChatColor.RED+"Usage: /hardmining reload");
				return true;
			}
			if(args.length > 0) {
				if(args[0].equalsIgnoreCase("reload")) {
					this.reloadConfig();
					this.saveConfig();
					this.saveDefaultConfig();
				}
			}
		}
		return false;
	}
}
